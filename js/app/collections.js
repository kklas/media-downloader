/* global Backbone */
var app = app || {}
app.collections = {}

app.collections.Media = Backbone.Collection.extend({
  model: app.models.Media,

  initialize: function (models, options) {
    this.files = options.files

    this.files.each(addListener, this)

    this.listenTo(this.files, 'add', addListener)
    this.listenTo(this.files, 'remove', function (file) {
      this.stopListening(file)
      this.findMedia('name', file.get('name')).removeFile(file)
    })

    function addListener (file) {
      this.listenTo(file, 'change:name', function (file, name) {
        var previousName = file.previous('name')
        if (previousName) {
          this.findMedia('name', previousName).removeFile(file)
        }

        this.addFile(file)
      })

      this.addFile(file)
    }
  },

  addFile: function (file) {
    var name = file.get('name')
    if (!name) return

    var media = this.findMedia('name', name)
    if (!media) {
      media = new app.models.Media({ name: name })
      this.add(media)
    }
    media.addFile(file)
  },

  findMedia: function (attribute, value) {
    return this.find(function (media) { return media.get(attribute) === value })
  }
})

app.collections.Downloads = Backbone.Collection.extend({
  model: Backbone.Model.extend({ idAttribute: '_id' }),
  url: 'download',

  initialize: function (models, options) {
    this.files = options.files
    this.listenTo(this, 'add', function (download) {
      var file = this.files.find(function (file) {
        return file.get('_id') === download.get('fileId')
      })

      if (!file) return
      file.updateDlState({ progress: download.get('progress') || 0 })
      this.downloadFile(file)

      this.listenTo(download, 'change:progress', function (model, progress) {
        file.updateDlState({ progress: progress })
      })
    })
  },

  downloadFile: function (file) {
    var fileId = file.get('_id')
    if (!fileId) return

    if (this.some(function (download) {
      return download.get('fileId') === fileId
    })) return

    this.create({
      'fileId': fileId,
      'progress': 0
    }, { wait: true })
  }
})

app.collections.Files = Backbone.Collection.extend({
  model: app.models.File,
  url: '/file'
})
