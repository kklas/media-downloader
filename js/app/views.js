/* global Backbone, _, $ */
var app = app || {}
app.views = {}

app.views.Navigation = Backbone.View.extend({
  el: '#navigation',
  items: ['sort', 'download'],

  initialize: function () {
    this.views = {}
  },

  render: function () {
    if (!this.views) this.views = {}
    this.$el.html('')
    _.each(this.items, function (item) {
      var el = $(
        '<li role="presentation"><a href="javascript:void(0)">' + capitalize(item) + '</a></li>'
      )
      this.$el.append(el)
      this.views[item] = el
      el.click(this.trigger.bind(this, 'click', item))
    }, this)

    function capitalize (string) {
      return string.charAt(0).toUpperCase() + string.slice(1)
    }
  },

  setActive: function (item) {
    if (this.active) this.active.removeClass('active')

    var $item = this.views[item]
    if (!$item) return
    $item.addClass('active')
    this.active = $item
  }

})

app.views.SortView = Backbone.View.extend({
  events: {
    'click .btn#save-all': 'saveAll'
  },

  initialize: function () {
    this.forms = {}

    this.$el.html('<ul class="list-group" id="file-forms"></ul>')
    this.$el.append('<button class="btn btn-primary" id="save-all">Save All</button>')

    this.collection.each(function (file) {
      addListener.call(this, file)
      this.addForm(file)
    }, this)

    this.listenTo(this.collection, 'add', function (file) {
      addListener.call(this, file)
      this.addForm(file)
    })
    this.listenTo(this.collection, 'remove', function (file) {
      this.stopListening(file)
      this.removeForm(file)
    })

    $('#app').append(this.$el)

    function addListener (file) {
      this.listenTo(file, 'change:name', function (file, name) {
        if (name) this.removeForm(file)
        else this.addForm(file)
      })
    }
  },

  saveAll: function () {
    _.each(this.forms, function (form) {
      form.save()
    })
  },

  remove: function () {
    _.each(this.forms, function (form) {
      form.remove()
    })
    this.forms = {}
    Backbone.View.prototype.remove.apply(this)
  },

  addForm: function (file) {
    var id = file.get('_id')
    if (file.get('name') || this.forms[id]) return

    var form = new app.views.FileFormView({model: file})
    this.forms[id] = form
    form.render()
    this.$el.find('ul').append(form.el)
  },

  removeForm: function (file) {
    var id = file.get('_id')
    var form = this.forms[id]
    if (form) {
      this.forms[id].remove()
      delete this.forms[id]
    }
  }
})

app.views.FileFormView = Backbone.View.extend({
  tagName: 'li',
  className: 'list-group-item',

  render: function () {
    var data = this.model.parseData()
    if (data.category === 'movie') {
      this.$el.append(this.movieTemplate({
        path: this.model.get('path'),
        year: data.year,
        name: data.name,
        category: data.category
      }))
    } else {
      this.$el.append(this.tvTemplate({
        path: this.model.get('path'),
        name: data.name,
        season: data.season,
        episode: data.episode,
        category: data.category
      }))
    }

    return this
  },

  save: function () {
    var saveBtn = this.$el.find('.btn.save')
    saveBtn.addClass('disabled')
    var data = {
      name: this.$el.find('input.name').val(),
      category: this.$el.find('input.category').val()
    }
    if (data.category === 'tv') {
      data.season = parseInt(this.$el.find('input.season').val())
      data.episode = parseInt(this.$el.find('input.episode').val())
    } else {
      data.year = parseInt(this.$el.find('input.year').val())
    }
    this.model.save(data, {
      wait: true,
      error: function () {
        console.log('failed saving')
        saveBtn.removeClass('disabled')
      }
    })
  },
  events: {
    'click .btn.save': 'save'
  },

  tvTemplate: _.template($('#sort-file-tv-form-template').html()),
  movieTemplate: _.template($('#sort-file-movie-form-template').html())
})

app.views.DownloadView = Backbone.View.extend({
  initialize: function () {
    this.mediaViews = []

    this.$el.html('<ul class="list-group" id="media"></ul>')

    this.collection.each(function (media) {
      this.addMedia(media)
    }, this)

    this.listenTo(this.collection, 'add', function (media) {
      this.addMedia(media)
    })

    $('#app').append(this.$el)
  },

  remove: function () {
    _.each(this.mediaViews, function (view) {
      view.remove()
    })
    this.mediaViews = {}
    Backbone.View.prototype.remove.apply(this)
  },

  addMedia: function (media) {
    var mediaView = new app.views.DownloadMediaView({model: media})
    this.mediaViews.push(mediaView)
    mediaView.render()
    this.$el.find('ul#media').append(mediaView.el)
  }
})

app.views.DownloadMediaView = Backbone.View.extend({
  tagName: 'li',
  className: 'list-group-item',
  template: _.template($('#download-media-list-template').html()),
  itemTemplate: _.template($('#download-media-item-template').html()),

  initialize: function () {
    this.listenTo(this.model, 'change:files', function (model, files) {
      this.renderFiles(files)
    })
  },

  render: function (files) {
    this.$el.html('')
    this.$el.append(this.template({ name: this.model.get('name') }))
    this.renderFiles(this.model.get('files'))
  },

  renderFiles: function (files) {
    var self = this

    var $fileList = this.$el.find('ul.file-list')
    $fileList.html('')
    _.each(files, function (file) {
      var $listItem = renderFileItem(file)
      $fileList.append($listItem)

      this.stopListening(file, 'change:dlState')
      this.listenTo(file, 'change:dlState', function (model, value) {
        var dlBtn = $listItem.find('.btn.download')

        var dlPercent = Math.round(value.progress * 10000) / 100
        $listItem.find('.progress-bar').css('width', dlPercent + '%')
        if (value.progress > 0) dlBtn.addClass('disabled')
      })
    }, this)

    function renderFileItem (file) {
      var dlProgress = file.dlState.progress || 0
      var dlPercent = Math.round(dlProgress * 10000) / 100

      var data = {
        path: file.get('path'),
        dlPercent: dlPercent
      }

      var el = $(self.itemTemplate(data))

      var dlBtn = el.find('.btn.download')
      dlBtn.click(function () {
        if (dlBtn.hasClass('disabled')) return
        app.data.downloads.downloadFile(file)
        dlBtn.addClass('disabled')
      })
      if (dlProgress > 0) dlBtn.addClass('disabled')

      return el
    }
  },

  download: function () {
    this.$el.find('.btn.download').click()
  },

  events: {
    'click .btn.download-all': 'download'
  }
})
