/* globals Backbone */
var app = app || {}
app.models = {}

app.models.Media = Backbone.Model.extend({
  addFile: function (file) {
    var id = file.get('_id')
    var files = this.get('files') || {}
    if (!files[id]) {
      files[id] = file
      this.set('files', files, {silent: true})
      this.trigger('change:files', this, files)
    }
  },

  removeFile: function (file) {
    var id = file.get('_id')
    var files = this.get('files') || {}
    if (files[id]) {
      delete files[id]
      this.set('files', files, {silent: true})
      this.trigger('change:files', this, files)
    }
  }
})

app.models.File = Backbone.Model.extend({
  idAttribute: '_id',

  initialize: function () {
    this.dlState = {}
  },

  parseData: function () {
    var path = this.get('path')
    var splitPath = path.split('/')

    var movieRe = /([^/]+)\.(\d\d\d\d)\./
    var tvRe = /([^/]+)\.(s(\d\d)e(\d\d)|s\d\d)\./i

    var data = {}
    var cat = splitPath.shift() === 'tv' ? 'tv' : 'movie'
    data.category = cat

    var re = []
    if (cat === 'movie') {
      re = movieRe.exec(path)
      data.name = re[1]
      data.year = re[2]
    } else {
      re = tvRe.exec(path)
      var name = re[1]
      var season = re[2]

      var episode = null
      if (season.length === 6) {
        episode = parseInt(season.slice(4, 6))
      }
      season = parseInt(season.slice(1, 3))

      data.name = name
      data.season = season
      data.episode = episode
    }

    return data
  },

  updateDlState: function (data) {
    this.dlState = data
    this.trigger('change:dlState', this, data)
  }
})
