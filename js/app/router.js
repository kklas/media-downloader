/* global Backbone */

var app = app || {}

app.Router = Backbone.Router.extend({

  routes: {
    '': 'sort',
    'sort': 'sort',
    'download': 'download'
  },

  initialize: function (options) {
    this.navigationView = options.navigationView
    this.listenTo(this.navigationView, 'click', function (item) {
      this.navigate(item, { trigger: true })
    })
  },

  sort: function () {
    app.data.files.fetch()
    if (app.currentView) app.currentView.remove()
    app.currentView = new app.views.SortView({ collection: app.data.files })
    this.navigationView.setActive('sort')
  },

  download: function () {
    app.data.files.fetch()
    if (app.currentView) app.currentView.remove()
    app.currentView = new app.views.DownloadView({ collection: app.data.media })
    this.navigationView.setActive('download')
  }
})
