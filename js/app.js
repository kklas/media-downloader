/* global Backbone, $ */
var app = app || {}

$(function () {
  app.data = {}
  app.data.files = new app.collections.Files()
  app.data.downloads = new app.collections.Downloads(null, { files: app.data.files })
  app.data.media = new app.collections.Media(null, { files: app.data.files })

  app.navigationView = new app.views.Navigation()
  app.navigationView.render()

  app.router = new app.Router({ navigationView: app.navigationView })
  Backbone.history.start()

  fetchDownloads()
})

function fetchDownloads () {
  app.data.downloads.fetch()
  setTimeout(fetchDownloads, 1000)
}
