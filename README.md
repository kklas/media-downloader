**Warning: ** This is currently in development and experimental. This is not tested and many features like form validation are missing. Only use this if you know what you are doing.


**Note:** The backend is not provided here

## What it does: ##
The idea of this app is to have a backend which connects to a remote server and fetches a list of files that can be downloaded from the server. The available files are then listed on the frontend where they can be parsed and sorted. When this is done, files become available for download. The files are downloaded from the remote server to the machine hosting the backend. 

This can be useful for Home Theater PCs which are usually harder to interface with since most users prefer not to have keyboard and mouse connected to them. With this kind of setup, you can manage downloading of media files from a remote server to the HTPC using a remote PC.

## Demo preview: [http://kklas.bitbucket.org/media_downloader_demo/](http://kklas.bitbucket.org/media_downloader_demo/)##
When the demo is refreshed, the app state is essentialy reset because the demo is not connected to any kind of backend. Backbone collections have been modified using Backbone.localStorage to mock the backend.